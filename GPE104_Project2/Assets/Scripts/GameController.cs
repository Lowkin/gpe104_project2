﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController gc; // Singleton Game Controller object

    /* Public member variables */
    public bool continuousFire = false;

    public int maxLives; // Max lives a player can have

    public float playerMovementSpeed; // Create a public variable to control the speed the player moves.  Editable in Editor.
    public float playerRotationSpeed; // Create a public variable to control the speed the player rotates.  Editable in Editor.
    public float enemyDeadZoneDistance; // How far from the player enemy ships have to be to fire projectiles
    public float bulletLifeTime; // Variable to hold how long bullets last
    public float enemyMovementSpeed; // Variable to hold how fast ships move
    public float enemyRotationSpeed; // Variable to hold how fast ships rotate
    public float maxAngleMovement; // Variable to control the max angle between the ship and the enemy that the enemy ship will move 
    public float asteroidMovementSpeed; // Variable to hold how fast asteroids move
    public float spawnRateInSeconds; // how quickly we spawn enemies or asteroids
    public float projectileSpeed; // how fast bullets travel
    public float enemyRateOfFire; // how often the enemy ships can fire
    public float playerRateOfFire; // how often the player can fire
    public float asteroidRotationLow; // low end for asteroid rotation spin
    public float asteroidRotationHigh; // high end for asteroid rotation spin

    public GameObject goSpaceShip; // variable to store a reference to our spaceship game object.  Set in Editor
    public GameObject canvas; // variable to hold a reference to our canvas
    public GameObject playerLife; // variable to hold our UI prefab for a player life
    public GameObject gameOverUI; // variable to hold our game over UI element
    public Text scoreUI; // variable to hold our score gameobject
    public GameObject[] enemies; // array to hold all possible spawnable objects (Asteroids or Ships)
    public GameObject[] asteroidExplosions; // array to hold explosion prefabs to use when an asteroid is destroyed
    public GameObject[] shipExplosions; // array to hold exposion prefabs to use when a ship is destroyed
    public GameObject[] playerExplosions; // array to hold explosion prefabs to use when the player ship is destroyed
    public GameObject[] spawnLocations; // array to hold possible spawn locations for ships or asteroids
    public GameObject[] planetLocations; // array to hold possible spawn locations for background planets
    public GameObject[] planets; // array to hold our planet prefabs

    
    /* Private member variables */
    private bool gameOver = false; // boolean to hold the game state

    private int enemyCount = 0; // variable to count how many enemies or asteroids are currently active
    private int spawnLocation; // variable to hold the last spawn location we used (to avoid double spawning at one spot)
    private int currentLives; // variable to hold the current number of lives the player has
    private int playerScore = 0; // variable to hold player score

    private float spawnTimer = 0; // variable to hold how long its been since we last spawned an enemy or asteroid

    private GameObject[] uiLives; // Array to hold our UI heart elements so we can enable/disable them 

    private void Awake()
    {
        // If we don't have a GameController instance set our singleton to this instance
        if (gc == null)
            gc = this;
        // Otherwise delete this one
        else
            Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start () {

        uiLives = new GameObject[maxLives]; // initialize our UI lives array

        displayPlanets(); // Populate our background with planets
        initializeUI(); // initialize our UI

        Physics2D.IgnoreLayerCollision(9, 8);  // Ignore any collisions between the player and their own projectiles
        Physics2D.IgnoreLayerCollision(9, 11); // Ignore any collisions between enemy and player projectiles
        Physics2D.IgnoreLayerCollision(10, 11); // Ignore any collisions between enemy projectiles and enemy ships

        spawnTimer = spawnRateInSeconds; // Set our timer to our rate so we can immediately spawn
        currentLives = maxLives; // set our current lives to max lives
        spawnLocation = Random.Range(0, spawnLocations.Length); // set our initial spawn location randomly
        
	}
	
	// Update is called once per frame
	void Update () {

        // If we can now spawn another object
        if (spawnTimer >= spawnRateInSeconds && enemyCount < 3 && !gameOver)
        {
            Vector3 location = spawnLocations[spawnLocation].transform.position + ((Vector3)(Random.insideUnitCircle) * 3); // here we use a random spawn location and put a bit more randomness on top of it by picking a spot near it using Random.insideunitcircle
            Instantiate(enemies[Random.Range(0, enemies.Length)], location, Quaternion.identity); // Spawn a random enemy at a random spawn location
            enemyCount++; // increment the number of enemies we have active
            spawnLocation = spawnLocation == spawnLocations.Length - 1 ? 0 : spawnLocation + 1;  // set our next spawn location
            spawnTimer = 0; // set our timer back to 0 to start the cooldown of spawning an enemy
        }
        else
            spawnTimer += Time.deltaTime;

		// Check for the "Esc" key to exit our application
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit();
		}
	}

    // function for setting up our UI
    private void initializeUI()
    {
        for (int i = 0; i < maxLives; i++)
        {
            GameObject heart = Instantiate<GameObject>(playerLife, canvas.transform, false); // instantiate our UI element as a child of the canvase
            RectTransform rect = (RectTransform)heart.transform; // get a reference to our UI rectangle
            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x + i * 50, rect.anchoredPosition.y); // offset the UI element the correct amount
            uiLives[i] = heart; // add our UI heart to our array for UI control
        }
    }

    // function to populate our background
    private void displayPlanets()
    {
        int count = Random.Range(1, planetLocations.Length+1); // pick a random number of planets to display
        int index = Random.Range(0, planetLocations.Length); // pick a random spawn location to start with

        for (int i = 0; i < count; i++)
        {
            Vector3 location = planetLocations[index].transform.position + ((Vector3)(Random.insideUnitCircle) * 2); // here we use a random spawn location and put a bit more randomness on top of it by picking a spot near it using Random.insideunitcircle
            Instantiate(planets[Random.Range(0, planets.Length)], location, Quaternion.identity); // Spawn our random planet at the chosen start index
            index = index == planetLocations.Length - 1 ? 0 : index + 1;
        }
    }

    // function for enemy objects to let the game controller know they have been destroyed
    public void enemyDeath ()
    {
        enemyCount --; // decrement the number of enemies we have in our scene
    }

    // function to update player score
    public void incrementScore()
    {
        playerScore++;
        scoreUI.text = playerScore.ToString();
    }

    // Function to destroy all gameobjects tagged "Destroy On Death", reset the player position, and decrement player lives
    public void resetGameState()
    {
        // If we just lost our last life then we exit
        if (currentLives == 1)
        {
            Application.Quit();

            // set our game over UI active
            gameOverUI.SetActive(true);
            gameOver = true;
        }
        // if we still have lives to live reset our game state
        else
        {
            // remove a life
            currentLives--;

            // remove a life from the UI
            uiLives[currentLives].SetActive(false);

            // Instantiate an explosion where the ship was destroyed
            Instantiate(gc.playerExplosions[Random.Range(0, gc.playerExplosions.Length)], goSpaceShip.transform.position, Quaternion.identity);  // Spawn an explosion sprite

            // Get all gameobjects marked as "DestroyOnDeath"
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("DestroyOnDeath");

            // If we found enemies then we loop through them all and destroy each one
            if (enemies != null && enemies.Length > 0)
            {
                for (int i = 0; i < enemies.Length; i++)
                    Destroy(enemies[i]);
            }

            // Reset the player position
            goSpaceShip.transform.position = Vector3.zero;

            // Set our spawn timer and spawn locations back to 'starting' positions
            spawnTimer = spawnRateInSeconds; // Set our timer to our rate so we can immediately spawn

            spawnLocation = Random.Range(0, spawnLocations.Length); // set our initial spawn location randomly
        }
    }


}

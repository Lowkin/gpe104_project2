﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipController : MonoBehaviour {

    public GameObject bullet;

    private GameController gc; // Variable to hold our GameController reference
    private Transform bulletSpawn; // Our spawn location for ammo
    private float fireRate; // Variable to hold how long its been since we last fired


    // Use this for initialization
    void Start () {
        gc = GameObject.Find("GameController").GetComponent<GameController>() as GameController;  // Find our gamecontroller at startup
        bulletSpawn = transform.GetChild(0); // assign our bullet spawn location to our first (and only) child
        transform.up = (gc.goSpaceShip.transform.position - transform.position).normalized; // Set transform up so that the ship is facing our player when it spawns
    }
	
	// Update is called once per frame
	void Update () {
        float distanceToPlayer = Vector2.Distance(gc.goSpaceShip.transform.position, transform.position);  // Get the distance between the enemy and the player
        Vector3 directionToMove = (gc.goSpaceShip.transform.position - transform.position).normalized; // The direction the player ship is in relative to the enemy ship
        float angle = Vector2.Angle(directionToMove, transform.up); // the angle between the ships forward direction and the direction the player ship is        

        if (angle < gc.maxAngleMovement)
            transform.position += (directionToMove * gc.enemyMovementSpeed * Time.deltaTime); // move in the direction of the player ship        
        transform.up = Vector3.Lerp(transform.up, directionToMove, gc.enemyRotationSpeed * Time.deltaTime); // rotate the ship in the direction of the player at the specified speed

        if (distanceToPlayer > gc.enemyDeadZoneDistance && fireRate >= gc.enemyRateOfFire) { 
            Instantiate(bullet, bulletSpawn.position, transform.rotation); // Spawn a bullet at our spawn location
            fireRate = 0; // reset our firerate cooldown
        }
        else
            fireRate += Time.deltaTime; // Otherwise add our timepassed to the rate

        }

    // Function to handle what happens when we enter another trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Did we run into an asteroid? If so destroy it
        if (collision.gameObject.layer == 12)
        {
            Instantiate(gc.asteroidExplosions[Random.Range(0, gc.asteroidExplosions.Length)], collision.gameObject.transform.position, Quaternion.identity);  // Spawn an explosion sprite
            Destroy(collision.gameObject); // Destroy the other object
        }
    }

    // Function to handle what happens when this object is destroyed
    void OnDestroy()
    {     
        gc.enemyDeath();  // Let the GameController know this enemy is destroyed
    }
}

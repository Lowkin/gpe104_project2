﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

    private GameController gc; // Reference to our GameController
    private float speed; // variable to hold how fast we move

	// Use this for initialization
	void Start () {
        gc = GameObject.Find("GameController").GetComponent<GameController>() as GameController; // Get a reference to our GameController

        Destroy(gameObject, gc.bulletLifeTime); // Destroy this projectile after the set amount of time
        speed = gc.projectileSpeed; // Set our projectile speed
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += (transform.up * speed * Time.deltaTime); // Move projectile at the set speed
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If we have hit an Asteroid or Enemy Ship
        if (other.gameObject.layer == 10 || other.gameObject.layer == 12)
        {
            if (other.gameObject.layer == 10) // If we are a ship spawn ship explosion
                Instantiate(gc.shipExplosions[Random.Range(0, gc.shipExplosions.Length)], other.gameObject.transform.position, Quaternion.identity);  // Spawn an explosion sprite
            // otherwise spawn asteroid explosion
            else
                Instantiate(gc.asteroidExplosions[Random.Range(0, gc.asteroidExplosions.Length)], other.gameObject.transform.position, Quaternion.identity);  // Spawn an explosion sprite

            // if we are a player projectile then we get to add to score
            if (gameObject.layer == 9)
            {
                gc.incrementScore();
            }

            // After everything else is done destroy the objects
            Destroy(other.gameObject); // Destroy what we just hit
            Destroy(gameObject); // Destroy ourselves since we are a bullet
        }
    }
}

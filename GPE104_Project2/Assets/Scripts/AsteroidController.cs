﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

    private GameController gc; // Reference to our GameController
    private Vector3 direction; // variable to hold where we are going
    private float rotationRate = 50; // variable to hold rotation rate
    // Use this for initialization
    void Start () {
        gc = GameObject.Find("GameController").GetComponent<GameController>() as GameController;  // Find our gamecontroller at startup

        direction = (gc.goSpaceShip.transform.position - transform.position).normalized; // set our asteroids direction
        rotationRate = Random.Range(gc.asteroidRotationLow, gc.asteroidRotationHigh); // get our rotation rate randomly between the designers set values
    }
	
	// Update is called once per frame
	void Update () {
        transform.RotateAround(transform.position, Vector3.forward, rotationRate * Time.deltaTime); // Rotate our asteroid by random speed
        transform.position += (direction * gc.asteroidMovementSpeed * Time.deltaTime); // Move projectile at the set speed
    }

    // Function to handle what happens when we enter another trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Did we run into an enemy ship? If so destroy it
        if (collision.gameObject.layer == 10)
        {
            Instantiate(gc.shipExplosions[Random.Range(0, gc.shipExplosions.Length)], collision.gameObject.transform.position, Quaternion.identity);  // Spawn an explosion sprite
            Destroy(collision.gameObject); // Destroy the other object
        }
    }

    // Function to handle what happens when this object is destroyed
    void OnDestroy()
    {
        gc.enemyDeath();  // Let the GameController know this enemy is destroyed
    }
}

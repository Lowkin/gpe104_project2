﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public GameObject bullet; // Reference to our Amunition 

    private GameController gc; // Our reference to the GameController
    private float fireRate; // Variable to hold how long its been since we last fired
    public GameObject bulletSpawn; // Variable to hold our bullet spawn location
    public GameObject exhaust; // Variable to hold our exhaust gameobject

    // Use this for initialization
    void Start () {
        gc = GameObject.Find("GameController").GetComponent<GameController>() as GameController; // Get a reference to our GameController
        fireRate = gc.playerRateOfFire; // set our fireRate to the rate of fire so the player can fire immediately
	}
	
	// Update is called once per frame
	void Update () {
        exhaust.SetActive(false);
        // Check W and Up Arrow to move in the positive Y direction
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)){
            transform.position += (transform.up * gc.playerMovementSpeed * Time.deltaTime);
            exhaust.SetActive(true); // only set our exhaust active if we are going forward
        }
		// Check S and Down Arrow to move in the negative Y direction
		else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)){
            transform.position -= (transform.up * (gc.playerMovementSpeed * .5f) * Time.deltaTime);// Move our spaceship in the up direction by 1 unit
        }
        // Check A and Left Arrow to rotate to the left
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){
            transform.Rotate(Vector3.forward * gc.playerRotationSpeed * Time.deltaTime);
        }
        // Check D and Right Arrow to rotate to the right
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){
            transform.Rotate(-Vector3.forward * gc.playerRotationSpeed * Time.deltaTime);
        }

        // Check for the spacebar to fire a bullet if we can.  Also checks for continuous fire so we can hold down spacebar
        if ((Input.GetKeyDown(KeyCode.Space) || (Input.GetKey(KeyCode.Space) && gc.continuousFire) ) && fireRate >= gc.playerRateOfFire)
        {
            Instantiate(bullet, bulletSpawn.transform.position, transform.rotation); // Spawn a bullet at our spawn location
            fireRate = 0; // reset our fire rate to start the cooldown
        }
        else
            fireRate += Time.deltaTime; // Otherwise add our timepassed to the rate
    }

    // function to call if we enter a trigger
    void OnTriggerEnter2D(Collider2D collision)
    {
        // If we have hit an enemy ship, enemy projectile, or asteroid we let the game controller know
        if (collision.gameObject.layer == 10 || collision.gameObject.layer == 11 || collision.gameObject.layer == 12)
        {
            gc.resetGameState(); // reset game state
        }
    }

    // function to call if we exit a trigger
    private void OnTriggerExit2D(Collider2D collision)
    {
        // if we have left the game area
        if (collision.gameObject.layer == 13)
        {
            gc.resetGameState(); // reset game state
        }
    }
}

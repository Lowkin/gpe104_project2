﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAreaDestruction : MonoBehaviour {

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    // When we leave the game area we destory ourselves
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 13)
        {
            // Destroy the gameObject
            Destroy(gameObject);
        }
    }
}
